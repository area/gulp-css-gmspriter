var gulp = require('gulp'),  
    gm  = require('gulp-gm');
    compass=require('gulp-compass'),//compass编译sass
	  cssSprite=require('gulp-css-spritesmith'),
    runSequence = require('run-sequence'),//队列执行任务
    rename=require('gulp-rename'),
    browserSync = require('browser-sync').create();

var iconsPath="images/icons/*.png"; //需要合成图片的切片图
var spriteIconsPath="images/slice/";//合成完图片路径
var cssFile="stylesheets/mobileicons.css";

/**css-sprite-android**/
gulp.task('gm-android', function() { 
   gulp.src(iconsPath)
  .pipe(gm(function (gmfile, done) {
    gmfile.size(function (err, size) {
      done(null, gmfile.resize(
        size.width * 0.5,
        size.height * 0.5
      ));
 
    });
  }))
 
  .pipe(gulp.dest(spriteIconsPath));
});

/** css-sprite-ios */
gulp.task('gm-ios', function() { 
   gulp.src(iconsPath)
  .pipe(gm(function (gmfile, done) {
    gmfile.size(function (err, size) {
      done(null, gmfile.resize(
        size.width * 1,
        size.height * 1
      ));
 
    });
  }))
  .pipe(rename(function (path) {
    path.basename += "@2x";
   }))
  .pipe(gulp.dest(spriteIconsPath));
});

gulp.task("gm-icons",function(){
    gulp.start("gm-android");
    gulp.start("gm-ios");
});
// 自动雪碧图
// autoSprite, with media query
gulp.task('autoSprite',['gm-icons'], function() {
    gulp.src(cssFile).pipe(cssSprite({
        // sprite背景图源文件夹，只有匹配此路径才会处理，默认 images/slice/
        imagepath: 'images/slice',
        // 映射CSS中背景路径，支持函数和数组，默认为 null
        imagepath_map: null,
        // 雪碧图输出目录，注意，会覆盖之前文件！默认 images/
        spritedest: 'images/',
        // 替换后的背景路径，默认 ../images/
        spritepath: '../images/',
        // 各图片间间距，如果设置为奇数，会强制+1以保证生成的2x图片为偶数宽高，默认 0
        padding: 20,
        // 是否使用 image-set 作为2x图片实现，默认不使用
        useimageset: false,
        // 是否以时间戳为文件名生成新的雪碧图文件，如果启用请注意清理之前生成的文件，默认不生成新文件
        newsprite: false,
        // 给雪碧图追加时间戳，默认不追加
        spritestamp: false,
        // 在CSS文件末尾追加时间戳，默认不追加
        cssstamp: false,
         // 默认使用二叉树最优排列算法
        //algorithm: 'top-down'
    }))
    .pipe(gulp.dest('publish/'));
});



gulp.task('compass', function() {
  console.log('compass watch...');
  return gulp.src("sass/**/*.scss")
    .pipe(compass({
      config_file: './config.rb',
      css: 'stylesheets',
      sass: 'sass'
    }))

});

gulp.task("watch",function(){
    gulp.watch("sass/**/*.scss",function(){
        runSequence(
            'compass',
            'autoSprite'
            );
    });
});


// 静态服务器
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('default',['browser-sync','watch']);