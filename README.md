#gulp-css-gmspriter

简单的移动端css切片图合成工具

#依赖的工具库：
nodejs<br/>

GraphicsMagick 下载地址：http://www.graphicsmagick.org/<br /> 

python 下载地址：https://www.python.org/

#可选的工具库
compass+ruby+sass

#主要实现的功能

1.将原型图片进行处理，需要合成的图片目录images/icons/,输出到images/slice/目录下<br /> 

  images/icons/目录存放2x原型图片<br /> 
  ![输入图片说明](http://git.oschina.net/uploads/images/2016/0108/174901_0585b417_331468.png "在这里输入图片标题")

  images/slice/目录生成1x，2x图片<br /> 
![输入图片说明](http://git.oschina.net/uploads/images/2016/0108/180037_4f0c6597_331468.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0108/180053_9f9769c5_331468.png "在这里输入图片标题")
2.使用gm工具进行图片缩放<br /> 

3.使用gulp+gulp-css-spritersmith插件 进行css代码片段的图片合成<br /> 

4.输出目录publish/ <br />


#优势
1.不需要更改原来的css文件代码<br /> 

2.可以配置需要合成的图片源路径，比如：images/icons<br /> 

3.自动添加media query 移动设备查询代码片段，适应不同的屏幕材质（比如retina）<br /> 


#缺点
1.需要手动输入源切片图的css代码：
   ```
.m-icon-fail{
    background: url(#{$icon_1_path}/m-pay-err.png) no-repeat;
    width:100px;
    height:100px;
}


.m-icon-success{
    background: url(#{$icon_1_path}/m-suc.png) no-repeat;
    width:28px;
    height:28px;
}




```
#合成后的代码
```
.m-icon-fail { background: no-repeat;background-position: 0 0; width: 100px; height: 100px; }

.m-icon-success { background: no-repeat;background-position: 0 -183px; width: 28px; height: 28px; }


```

#示例
下载zip后，执行npm install,再执行gulp
如果没有compass环境，请将gulpfile.js中的compass的配置做相应调整，改成css监听